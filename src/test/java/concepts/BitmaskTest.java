package concepts;

import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by mauricio on 4/2/16.
 */
public class BitmaskTest {
    /**
     * This method checks if the indexed bit is turned on
     */
    @Test
    public void checkTurnedBitTest() {
        int value = 18; // 10010
        int index = 4;
        int mask = 1 << index; // Shifts 00001 four places to left -> 10000
        //System.out.println(mask);
        Assert.assertEquals(16, value & mask);
    }

    /**
     * This method asserts that a shift to left is equals to multiply by 2, a shift to right is division by 2
     */
    @Test
    public void checkMultiplicationsBitTest() {
        int value = 18; // 10010
        int multiplied = value << 1; // Shifts 10010 to left -> 100100 (multiply by 2)
        Assert.assertEquals(36, multiplied);

        int divided = value >> 1; // Shifts 10010 to right -> 1001 (divide by 2)
        Assert.assertEquals(9, divided);
    }

    /**
     * This method asserts an operation to set a bit on
     */
    @Test
    public void setIndexedBitTest() {
        int value = 18; // 10010
        int index = 2;
        int mask = 1 << index; // Shifts 1 two spaces left -> 100
        Assert.assertEquals(22, value | mask);

    }

    /**
     * This test clears the bit of an index
     */
    @Test
    public void clearBitTest() {
        int value = 18; // 10010
        int index = 1;
        int mask = ~ (1 << index); // Shifts 1 one spaces left -> 10 -> Then got its complement -> ...1101
        Assert.assertEquals(16, value & mask);
    }

    /**
     * This test toggles the bit of an index
     */
    @Test
    public void toggleBitTest() {
        int value = 18; // 10010
        int index = 2;  // Toggles the third bit
        int mask = 1 << index; // Shifts 1 two spaces left -> 100
        Assert.assertEquals(22, value ^ mask); // XOR
    }

    /**
     * This test finds the least significant bit of a number
     */
    @Test
    public void findLeastSignificantBitTest() {
        int value = 18; // 10010
        int twosComplement = -1 * value; // 1...01101 (twoComplement = Inverts every bit and adds 1)
        Assert.assertEquals(2, value & twosComplement); // XOR
    }

    /**
     * This test turns on all bits of set with size "n"
     */
    @Test
    public void turnBitsOnTest() {
        int n = 3;  // Set size
        int value = 1 << n; // 1000
        Assert.assertEquals(7, value - 1);  // 111
    }


}
