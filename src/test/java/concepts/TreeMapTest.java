package concepts;

import junit.framework.Assert;
import org.junit.Test;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by mauricio on 4/3/16.
 */
public class TreeMapTest {
    @Test
    public void treeMapTest() {
        TreeMap<Integer, Person> tree = new TreeMap();
        final Person mauricio = new Person("Mauricio", 29);
        tree.put(mauricio.getAge(), mauricio);
        final Person laerte = new Person("Laerte", 34);
        tree.put(laerte.getAge(), laerte);
        final Person clay = new Person("Clay", 34);
        tree.put(clay.getAge(), clay);
        final Person cicero = new Person("Cicero", 36);
        tree.put(cicero.getAge(), cicero);
        final Person joao = new Person("Joao", 33);
        tree.put(joao.getAge(), joao);
        final Person juliana = new Person("Juliana", 40);
        tree.put(juliana.getAge(), juliana);
        final Person fabio = new Person("Fabio", 35);
        tree.put(fabio.getAge(), fabio);
        System.out.println(tree);

        Assert.assertEquals(mauricio, tree.firstEntry().getValue());

        SortedMap<Integer, Person> subMap = tree.subMap(mauricio.getAge(), juliana.getAge());
        System.out.println(subMap);

        System.out.println(tree.headMap(juliana.getAge()).size());

        //tree.getde

    }
}

class Person implements Comparable {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }



    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        Person p = (Person) o;
        return getAge() - p.getAge();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (age != person.age) return false;
        return name != null ? name.equals(person.name) : person.name == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        return result;
    }
}
