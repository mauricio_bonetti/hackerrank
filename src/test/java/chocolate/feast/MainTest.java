// Solution for https://www.hackerrank.com/challenges/chocolate-feast
package chocolate.feast;

import junit.framework.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.stream.Collectors;

public class MainTest {

    @Test
    public void testReadInputs() throws UnsupportedEncodingException {
        String input = "3\n10 2 5\n12 4 4\n6 2 2";
        InputStream in = new ByteArrayInputStream(input.getBytes("UTF-8"));
        List<Main.ChocolatePurchase> purchases = Main.readInputs(in).collect(Collectors.toList());

        Assert.assertEquals(10, purchases.get(0).moneyQty);
        Assert.assertEquals(2, purchases.get(0).chocolatePrice);
        Assert.assertEquals(5, purchases.get(0).wrappersQty);

        Assert.assertEquals(12, purchases.get(1).moneyQty);
        Assert.assertEquals(4, purchases.get(1).chocolatePrice);
        Assert.assertEquals(4, purchases.get(1).wrappersQty);

        Assert.assertEquals(6, purchases.get(2).moneyQty);
        Assert.assertEquals(2, purchases.get(2).chocolatePrice);
        Assert.assertEquals(2, purchases.get(2).wrappersQty);
    }

    @Test
    public void testBuyChocolates() {
        Main.ChocolatePurchase purchase = new Main.ChocolatePurchase(10, 2, 5);
        //Assert.assertEquals(6, Main.buyChocolates(purchase));

        purchase = new Main.ChocolatePurchase(43203, 60, 5);
        Assert.assertEquals(899, Main.buyChocolates(purchase));
    }
}
