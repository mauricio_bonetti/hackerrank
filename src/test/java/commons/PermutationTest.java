package commons;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class PermutationTest {
    @Test
    public void findPermutationTest() {
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        long t1 = System.currentTimeMillis();
        final Set<List<Integer>> permutations = Permutation.findPermutation(integers);
        System.out.println(permutations);
        System.out.println(permutations.size());
        long t2 = System.currentTimeMillis();
        System.out.println("Time elapsed = " + (t2 - t1));
    }
}
