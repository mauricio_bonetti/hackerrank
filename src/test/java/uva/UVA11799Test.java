package uva;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.stream.Stream;

/**
 * Created by mauricio on 3/18/16.
 */
public class UVA11799Test {
    @Test
    public void findClownsSpeedTest() throws UnsupportedEncodingException {
        String input = "2\n5 9 3 5 2 6\n1 2";
        InputStream in = new ByteArrayInputStream(input.getBytes("UTF-8"));
        final Stream<String> cases = new UVA11799().findClownsSpeed(in);
        cases.forEach(System.out::println);
    }
}
