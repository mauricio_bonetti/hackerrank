package uva;

import junit.framework.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class UVA11559Test {
    @Test
    public void readInputTest() throws UnsupportedEncodingException {
        String input = "3 1000 2 3\n200\n0 2 2\n300\n27 3 20\n5 2000 2 4\n300\n4 3 0 4\n450\n7 8 0 13";
        InputStream in = new ByteArrayInputStream(input.getBytes("UTF-8"));
        List<Excursion> excursions = new UVA11559().readInput(in);

        Excursion excursion = excursions.get(0);
        Assert.assertEquals(3, excursion.getParticipantsQty());
        Assert.assertEquals(1000, excursion.getBudget());
        Assert.assertEquals(2, excursion.getHotelsQty());
        Assert.assertEquals(3, excursion.getWeeksQty());
        List<Hotel> hotels = excursion.getHotels();
        Assert.assertEquals(200, hotels.get(0).getPrice().intValue());
        Assert.assertEquals(0, (int) hotels.get(0).getBedsQty().get(0));
        Assert.assertEquals(2, (int) hotels.get(0).getBedsQty().get(1));
        Assert.assertEquals(2, (int) hotels.get(0).getBedsQty().get(2));
        Assert.assertEquals(300, hotels.get(1).getPrice().intValue());
        Assert.assertEquals(27, (int) hotels.get(1).getBedsQty().get(0));
        Assert.assertEquals(3, (int) hotels.get(1).getBedsQty().get(1));
        Assert.assertEquals(20, (int) hotels.get(1).getBedsQty().get(2));

        excursion = excursions.get(1);
        Assert.assertEquals(5, excursion.getParticipantsQty());
        Assert.assertEquals(2000, excursion.getBudget());
        Assert.assertEquals(2, excursion.getHotelsQty());
        Assert.assertEquals(4, excursion.getWeeksQty());
        hotels = excursion.getHotels();
        Assert.assertEquals(300, hotels.get(0).getPrice().intValue());
        Assert.assertEquals(4, (int) hotels.get(0).getBedsQty().get(0));
        Assert.assertEquals(3, (int) hotels.get(0).getBedsQty().get(1));
        Assert.assertEquals(0, (int) hotels.get(0).getBedsQty().get(2));
        Assert.assertEquals(4, (int) hotels.get(0).getBedsQty().get(3));
        Assert.assertEquals(450, hotels.get(1).getPrice().intValue());
        Assert.assertEquals(7, (int) hotels.get(1).getBedsQty().get(0));
        Assert.assertEquals(8, (int) hotels.get(1).getBedsQty().get(1));
        Assert.assertEquals(0, (int) hotels.get(1).getBedsQty().get(2));
        Assert.assertEquals(13, (int) hotels.get(1).getBedsQty().get(3));
    }

    @Test
    public void findCheapestPrice() {
        Excursion excursion = new Excursion(3, 1000, 2, 3);
        Hotel h1 = new Hotel("200");
        h1.addBedQty(0);
        h1.addBedQty(2);
        h1.addBedQty(2);
        excursion.addHotel(h1);
        Hotel h2 = new Hotel("300");
        h2.addBedQty(27);
        h2.addBedQty(3);
        h2.addBedQty(20);
        excursion.addHotel(h2);
        Assert.assertEquals(900, excursion.findCheapestPrice().get().intValue());

        excursion = new Excursion(5, 2000, 2, 4);
        h1 = new Hotel("300");
        h1.addBedQty(4);
        h1.addBedQty(3);
        h1.addBedQty(0);
        h1.addBedQty(4);
        excursion.addHotel(h1);
        h2 = new Hotel("450");
        h2.addBedQty(7);
        h2.addBedQty(8);
        h2.addBedQty(0);
        h2.addBedQty(13);
        excursion.addHotel(h2);
        Assert.assertEquals(false, excursion.findCheapestPrice().isPresent());
    }
}
