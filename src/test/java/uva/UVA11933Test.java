package uva;

import junit.framework.Assert;
import org.junit.Test;
import java.util.List;

public class UVA11933Test {

    @Test
    public void findTurnedOnBitsIndecesTest() {
        List<Integer> onBitsIndeces = new UVA11933().findTurnedOnBitsIndeces(10);
        Assert.assertEquals(1, (int) onBitsIndeces.get(0));
        Assert.assertEquals(3, (int) onBitsIndeces.get(1));
        onBitsIndeces = new UVA11933().findTurnedOnBitsIndeces(14);
        Assert.assertEquals(1, (int) onBitsIndeces.get(0));
        Assert.assertEquals(2, (int) onBitsIndeces.get(1));
        Assert.assertEquals(3, (int) onBitsIndeces.get(2));
        onBitsIndeces = new UVA11933().findTurnedOnBitsIndeces(7);
        Assert.assertEquals(0, (int) onBitsIndeces.get(0));
        Assert.assertEquals(1, (int) onBitsIndeces.get(1));
        Assert.assertEquals(2, (int) onBitsIndeces.get(2));
        /*
        System.out.println(new UVA11933().findTurnedOnBitsIndeces(Integer.MAX_VALUE));
        System.out.println(new UVA11933().findTurnedOnBitsIndeces(Integer.MIN_VALUE));
        System.out.println(new UVA11933().findTurnedOnBitsIndeces(Integer.MIN_VALUE + 1));
        System.out.println(new UVA11933().findTurnedOnBitsIndeces(1));
        System.out.println(new UVA11933().findTurnedOnBitsIndeces(Integer.MIN_VALUE + 2));
        System.out.println(new UVA11933().findTurnedOnBitsIndeces(Integer.MAX_VALUE - 1));
        */
    }

    @Test
    public void findATest() {
        Assert.assertEquals(2, new UVA11933().findDivision(true, 6));
        Assert.assertEquals(5, new UVA11933().findDivision(true, 7));
        Assert.assertEquals(9, new UVA11933().findDivision(true, 13));
    }

    @Test
    public void findBTest() {
        Assert.assertEquals(4, new UVA11933().findDivision(false, 6));
        Assert.assertEquals(2, new UVA11933().findDivision(false, 7));
        Assert.assertEquals(4, new UVA11933().findDivision(false, 13));
    }
}
