package uva;

import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UVA12247Test {
    /*
    @Test
    public void generateRoundsTest() {
        Game game = new Game();
        List<Integer> cards = new ArrayList<>(Arrays.asList(28, 51, 29, 50, 52));
        Round[][] rounds = game.generateRounds(cards);

        Assert.assertEquals(rounds[0][0].toString(), "Round{princessCard=28, princeCard=50}");
        Assert.assertEquals(rounds[0][1].toString(), "Round{princessCard=28, princeCard=52}");
        Assert.assertEquals(rounds[0][2].toString(), "Round{princessCard=28, princeCard=0}");
        Assert.assertEquals(rounds[1][0].toString(), "Round{princessCard=51, princeCard=50}");
        Assert.assertEquals(rounds[1][1].toString(), "Round{princessCard=51, princeCard=52}");
        Assert.assertEquals(rounds[1][2].toString(), "Round{princessCard=51, princeCard=0}");
        Assert.assertEquals(rounds[2][0].toString(), "Round{princessCard=29, princeCard=50}");
        Assert.assertEquals(rounds[2][1].toString(), "Round{princessCard=29, princeCard=52}");
        Assert.assertEquals(rounds[2][2].toString(), "Round{princessCard=29, princeCard=0}");
    }

    @Test
    public void generateGameTest() {
        Game game = new Game();
        List<Integer> cards = new ArrayList<>(Arrays.asList(28, 51, 29, 50, 52));
        Round[][] rounds = game.generateRounds(cards);
        game.generateGame(rounds);
        System.out.println(game);
    }
    */

    @Test
    public void getMinimumCardForPrinceVictoryTest() {
        Game g = Game.generate(new ArrayList<>(Arrays.asList(28, 51, 29, 50, 52)));
        Assert.assertEquals(30, g.getMinimumCardForPrinceVictory());

        g = Game.generate(new ArrayList<>(Arrays.asList(50, 26, 19, 10, 27)));
        Assert.assertEquals(-1, g.getMinimumCardForPrinceVictory());

        g = Game.generate(new ArrayList<>(Arrays.asList(10, 20, 30, 24, 26)));
        Assert.assertEquals(21, g.getMinimumCardForPrinceVictory());

        g = Game.generate(new ArrayList<>(Arrays.asList(46, 48, 49, 47, 50)));
        Assert.assertEquals(51, g.getMinimumCardForPrinceVictory());
    }
}
