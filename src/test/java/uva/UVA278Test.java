package uva;

import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by mauricio on 3/31/16.
 */
public class UVA278Test {
    @Test
    public void getMaximumChessPiecesTest() {
        char piece = 'r';
        int[][] board = new int[6][7];
        int maximummChessPieces = new UVA278().getMaximumChessPieces(piece, board);
        Assert.assertEquals(6, maximummChessPieces);
    }
}
