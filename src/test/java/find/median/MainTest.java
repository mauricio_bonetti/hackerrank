package find.median;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainTest {
    @Test(expected = IllegalArgumentException.class)
    public void testReadInput_ERROR() throws UnsupportedEncodingException {
        String input = "";
        InputStream in = new ByteArrayInputStream(input.getBytes("UTF-8"));
        new Main().readInput(in);
    }
/*
    // Check size correctness
    @MainTest(expected = IllegalArgumentException.class)
    public void testReadInput_ERROR_2() throws UnsupportedEncodingException {
        String input = "7\n0 1 2 4 6 5";
        InputStream in = new ByteArrayInputStream(input.getBytes("UTF-8"));
        new Main().readInput(in);
    }
*/
    @Test
    public void testReadInput_Succes() throws UnsupportedEncodingException {
        String input = "7\n0 1 2 4 6 5 3";
        InputStream in = new ByteArrayInputStream(input.getBytes("UTF-8"));
        final List<Integer> numbers = new Main().readInput(in).collect(Collectors.toList());
        Assert.assertEquals(0, numbers.get(0).intValue());
        Assert.assertEquals(1, numbers.get(1).intValue());
        Assert.assertEquals(2, numbers.get(2).intValue());
        Assert.assertEquals(4, numbers.get(3).intValue());
        Assert.assertEquals(6, numbers.get(4).intValue());
        Assert.assertEquals(5, numbers.get(5).intValue());
        Assert.assertEquals(3, numbers.get(6).intValue());
    }

    @Test
    public void testFindMedian() {
        Stream<Integer> numbers = Stream.of(0, 1, 2, 4, 6, 5, 3);
        final int median = new Main().findMedian(numbers);
        Assert.assertEquals(3, median);
    }
}
