package utopian.tree;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by mauricio on 3/3/16.
 */
public class MainTest {

    @Test
    public void testReadInputs_SUCCESS() throws UnsupportedEncodingException {
        String input = "3\n0\n1\n4";
        InputStream in = new ByteArrayInputStream(input.getBytes("UTF-8"));
        final List<UtopianTree> trees = new Main().readInputs(in).collect(Collectors.toList());
        Assert.assertEquals(3, trees.size());
        Assert.assertEquals(0, trees.get(0).getCycles());
        Assert.assertEquals(1, trees.get(1).getCycles());
        Assert.assertEquals(4, trees.get(2).getCycles());
    }

    @Test
    public void testGetHeight_SUCCESS() {
        Assert.assertEquals(1, new UtopianTree(0).getHeight());
        Assert.assertEquals(2, new UtopianTree(1).getHeight());
        Assert.assertEquals(7, new UtopianTree(4).getHeight());
    }
}
