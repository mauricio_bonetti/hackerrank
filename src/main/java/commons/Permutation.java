package commons;

import java.util.*;
import static java.util.Collections.swap;

/**
 * This class implements routines to perform permutations
 */
public class Permutation {

    /**
     * Finds all permutations of elements from list
     * @param list
     * @param <T>
     * @return Set containing lists of every permutation
     */
    public static <T> Set<List<T>> findPermutation(List<T> list) {
        int length = list.size();
        int[] control = new int[length];
        int i = 1;
        int j;
        Set<List<T>> permutations = new HashSet<>();
        permutations.add(list);
        ArrayList<T> permutation = new ArrayList<>(list);
        while (i < length) {
            if (control[i] < i) {
                j = i % 2 != 0 ? control[i] : 0;
                swap(permutation, i, j);
                control[i]++;
                i = 1;
                permutations.add(permutation);
                permutation = new ArrayList<>(permutation);
            } else {
                control[i] = 0;
                i++;
                System.gc();
            }
        }
        return permutations;
    }
}
