// Solution for https://www.hackerrank.com/challenges/utopian-tree
package utopian.tree;

import java.io.InputStream;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by mauricio on 3/3/16.
 */
public class Main {
    public Stream<UtopianTree> readInputs(InputStream in) {
        Scanner sc = new Scanner(in);
        final int treesQty = sc.nextInt();
        return IntStream.range(0, treesQty).mapToObj(i -> new UtopianTree(sc.nextInt()));
    }

    public static void main(String... args) {
        final Main main = new Main();
        final Stream<UtopianTree> trees = main.readInputs(System.in);
        trees.forEach(System.out::println);
    }
}

class UtopianTree {
    private int cycles;

    public UtopianTree(int cycles) {
        this.cycles = cycles;
    }

    public int getCycles() {
        return cycles;
    }

    public int getHeight() {
        return IntStream.range(0, cycles + 1)
                .reduce(0, (height, cycle) -> cycle % 2 == 0 ? height + 1 : height * 2);
    }

    @Override
    public String toString() {
        return Integer.toString(getHeight());
    }
}
