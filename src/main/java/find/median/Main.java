// Solution for https://www.hackerrank.com/challenges/find-median
package find.median;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String... args) {
        final Main main = new Main();
        final Stream<Integer> numbers = main.readInput(System.in);
        System.out.println(main.findMedian(numbers));
    }

    public int findMedian(Stream<Integer> numbers) {
        final List<Integer> numbersList = numbers.sorted().collect(Collectors.toList());
        return numbersList.get(numbersList.size() / 2);
    }

    public Stream<Integer> readInput(InputStream in) {
        try {
            Scanner sc = new Scanner(in);
            final int size = sc.nextInt();
            if (size % 2 == 0) {
                throw new IllegalArgumentException("Size of array is even!!!");
            }
            sc.nextLine();
            final String arrayString = sc.nextLine();
            // TODO: Como validar o tamanho do array sem perder o stream?!
            return Arrays.stream(arrayString.split(" ")).map(num -> Integer.parseInt(num));
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
