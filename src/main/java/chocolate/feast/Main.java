// Solution for https://www.hackerrank.com/challenges/chocolate-feast
package chocolate.feast;

import java.io.InputStream;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {

    public static void main(String... args) {
        Stream<ChocolatePurchase> chocolatePurchases = readInputs(System.in);
        chocolatePurchases.mapToInt(testCase -> buyChocolates(testCase)).forEach(System.out::println);
    }

    public static int buyChocolates(ChocolatePurchase chocolatePurchase) {
        int chocolates = chocolatePurchase.moneyQty / chocolatePurchase.chocolatePrice;
        int x = chocolates;
        int total = chocolates;
        while (x >= chocolatePurchase.wrappersQty) {
            x = x / chocolatePurchase.wrappersQty;
            total += x;
        }
        return total;
    }

    public static Stream<ChocolatePurchase> readInputs(InputStream in) {
        Scanner sc = new Scanner(in);
        int testsQty = Integer.parseInt(sc.nextLine());
        return IntStream.range(0, testsQty).mapToObj(i -> new ChocolatePurchase(sc.nextInt(), sc.nextInt(), sc.nextInt()));
    }

    static class ChocolatePurchase {
        int moneyQty, chocolatePrice, wrappersQty;

        public ChocolatePurchase(int moneyQty, int chocolatePrice, int wrappersQty) {
            this.moneyQty = moneyQty;
            this.chocolatePrice = chocolatePrice;
            this.wrappersQty = wrappersQty;
        }

    }
}
