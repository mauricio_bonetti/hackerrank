// Solution for https://uva.onlinejudge.org/index.php?option=onlinejudge&page=show_problem&problem=2595
package uva;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.IntStream;

public class UVA11559 {
    public static void main(String... args) {
        UVA11559 p = new UVA11559();
        List<Excursion> excursions = p.readInput(System.in);
        excursions.stream()
                .map(excursion -> excursion.findCheapestPrice())
                .forEach(cheapestPrice -> System.out.println(cheapestPrice.isPresent() ? cheapestPrice : "stay home"));
    }

    public List<Excursion> readInput(InputStream in) {
        Scanner sc = new Scanner(in);
        List<Excursion> excursions = new ArrayList<>();
        do {
            Excursion excursion = new Excursion(sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt());
            IntStream.range(0, excursion.getHotelsQty()).forEach(hotelIndex -> {
                sc.nextLine();
                Hotel hotel = new Hotel(sc.nextLine());
                IntStream.range(0, excursion.getWeeksQty()).forEach(weekIndex -> {
                    hotel.addBedQty(sc.nextInt());
                });
                excursion.addHotel(hotel);
            });
            excursions.add(excursion);
        } while (sc.hasNextLine());
        return excursions;
    }
}

class Excursion {
    private int participantsQty;
    private int budget;
    private int hotelsQty;
    private int weeksQty;
    private List<Hotel> hotels;

    public Excursion(int participantsQty, int budget, int hotelsQty, int weeksQty) {
        this.participantsQty = participantsQty;
        this.budget = budget;
        this.hotelsQty = hotelsQty;
        this.weeksQty = weeksQty;
        hotels = new ArrayList<>();
    }

    public Optional<BigDecimal> findCheapestPrice() {
        Optional<BigDecimal> optionalCost = Optional.empty();
        BigDecimal cheapestValue = new BigDecimal(Integer.MAX_VALUE).setScale(2);
        List<Hotel> hotels = getHotels();
        for (Hotel hotel: hotels) {
            if (hotel.isOnBudget(getBudget(), getParticipantsQty())
                    && hotel.canAccommodateEverybody(getParticipantsQty())) {
                BigDecimal cost = hotel.findCost(getParticipantsQty());
                if (cost.compareTo(cheapestValue) < 0) {
                    cheapestValue = cost;
                    optionalCost = Optional.of(cheapestValue);
                }
            }
        }
        return optionalCost;
    }

    public void addHotel(Hotel hotel) {
        hotels.add(hotel);
    }

    public int getParticipantsQty() {
        return participantsQty;
    }

    public int getBudget() {
        return budget;
    }

    public int getHotelsQty() {
        return hotelsQty;
    }

    public int getWeeksQty() {
        return weeksQty;
    }

    public List<Hotel> getHotels() {
        return hotels;
    }
}

class Hotel {
    private BigDecimal price;
    private List<Integer> bedsQty;

    public Hotel(String price) {
        this.price = new BigDecimal(price).setScale(2);
        this.bedsQty = new ArrayList<>();
    }

    public void addBedQty(int bedQty) {
        bedsQty.add(bedQty);
    }

    public BigDecimal getPrice() {
        return price;
    }

    public List<Integer> getBedsQty() {
        return bedsQty;
    }

    public boolean isOnBudget(int budget, int participantsQty) {
        return new BigDecimal(budget).compareTo(price.multiply(new BigDecimal(participantsQty))) >= 0;
    }

    public boolean canAccommodateEverybody(int participantsQty) {
        return getBedsQty().stream().anyMatch(bedQty -> bedQty >= participantsQty);
    }

    public BigDecimal findCost(int participantsQty) {
        return getPrice().multiply(new BigDecimal(participantsQty));
    }
}