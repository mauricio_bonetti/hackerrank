package uva;

import commons.Permutation;

import java.util.*;

public class UVA732 {

    public Set<String> findOperations(String word, String target) {
        Set<String> validOperations = findValidOperations(word);
        Set<String> rightOperations = new HashSet<>();
        for (String operation : validOperations) {
            if (operationGeneratesTarget(operation, word, target)) {
                rightOperations.add(operation);
            }
        }
        return rightOperations;
    }

    private boolean operationGeneratesTarget(String operation, String word, String target) {
        Stack<String> aux = new Stack<>();
        for (int i = 0; i < operation.length(); i++) {
            if (operation.charAt(i) == 'i') {
                String c = String.valueOf(word.charAt(0));
                aux.push(c);
                word = word.substring(1, word.length());
            } else {
                if (!aux.isEmpty()) {
                    String c = aux.pop();
                    word += c;
                }
            }
        }
        return word.equals(target);
    }

    public Set<String> findValidOperations(String word) {
        int length = word.length();
        List<String> operationsSource = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            operationsSource.add("i");
            operationsSource.add("o");
        }
        Set<List<String>> permutations = Permutation.findPermutation(operationsSource);
        Set<String> validOperations = new HashSet<>();
        for (List<String> permutation : permutations) {
            validOperations.add(Arrays.toString(permutation.toArray()));
        }
        return validOperations;
    }
}
