// Solution for https://uva.onlinejudge.org/external/117/11799.pdf
package uva;

import java.io.InputStream;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class UVA11799 {

    public static void main(String... args) {
        new UVA11799().findClownsSpeed(System.in).forEach(System.out::println);
    }

    public Stream<String> findClownsSpeed(InputStream in) {
        Scanner sc = new Scanner(in);
        int testsQtys = sc.nextInt();
        int[] testCaseIndex = {0};
        Stream<String> clownSpeeds = IntStream.range(testCaseIndex[0], testsQtys) // foreach testCase
                .map(i -> sc.nextInt()) // students quantity
                .map(studentsQty -> IntStream.range(0, studentsQty)
                            .map(student -> sc.nextInt())   // student speed
                            .max()  // max speed is the clown speed
                            .getAsInt()
                )
                .mapToObj(clownSpeed -> "Case " + ++testCaseIndex[0] + ": " + clownSpeed);  // Test case result
        return clownSpeeds;
    }
}
