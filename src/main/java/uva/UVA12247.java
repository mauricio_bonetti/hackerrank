package uva;

import java.io.InputStream;
import java.util.*;

public class UVA12247 {
    public static void main(String... args) {
        List<List<Integer>> games = new UVA12247().readInputs(System.in);
        for (List<Integer> game : games) {
            Game g = Game.generate(game);
            System.out.println(g.getMinimumCardForPrinceVictory());
        }
    }

    public List<List<Integer>> readInputs(InputStream in) {
        Scanner sc = new Scanner(System.in);
        List<List<Integer>> games = new ArrayList<>();
        for (;;) {
            int n1 = sc.nextInt();
            if (n1 == 0) {
                break;
            }
            int n2 = sc.nextInt();
            int n3 = sc.nextInt();
            int n4 = sc.nextInt();
            int n5 = sc.nextInt();
            List<Integer> game = new ArrayList<>(Arrays.asList(n1, n2, n3, n4, n5));
            games.add(game);
        }
        return games;
    }
}

/**
 * This class encapsulates all possible plays
 */
class Game {
    private List<Play> plays;

    private Game() {
        this.plays = new ArrayList<>();
    }

    public static Game generate(List<Integer> cards) {
        Round[][] rounds = generateRounds(cards);
        Game game = new Game();
        for (int i = 0; i < 3; i++) {
            Play play = new Play();
            play.addRound(rounds[0][i]);
            play.addRound(rounds[1][(i + 1) % 3]);
            play.addRound(rounds[2][(i + 2) % 3]);
            game.plays.add(play);
        }
        for (int i = 0; i < 3; i++) {
            Play play = new Play();
            play.addRound(rounds[0][(i + 2) % 3]);
            play.addRound(rounds[1][(i + 1) % 3]);
            play.addRound(rounds[2][i]);
            game.plays.add(play);
        }
        return game;
    }

    public int getMinimumCardForPrinceVictory() {
        int minimumCardForPrinceVictory = Integer.MIN_VALUE;
        for (Play play : plays) {
            Optional<Integer> m = play.getMinimumCardForPrinceVictory();
            if (!m.isPresent()) {
                return -1;
            }
            int minimumCard = m.get();
            if (minimumCard > minimumCardForPrinceVictory) {
                minimumCardForPrinceVictory = minimumCard;
            }
        }
        minimumCardForPrinceVictory = checkDeckConstraints(minimumCardForPrinceVictory);
        return minimumCardForPrinceVictory;
    }

    /**
     * Checks if card number is not already chosen. If so, gets the next number towards decks end
     * @param minimumCardForPrinceVictory
     * @return
     */
    private int checkDeckConstraints(int minimumCardForPrinceVictory) {
        for (Play play : plays) {
            for (Round round : play.getRounds()) {
                if (round.getPrinceCard() == minimumCardForPrinceVictory
                        || round.getPrincessCard() == minimumCardForPrinceVictory) {
                    if (minimumCardForPrinceVictory <= 52) {
                        return checkDeckConstraints(minimumCardForPrinceVictory + 1);
                    }
                    return -1;
                }
            }
        }
        return minimumCardForPrinceVictory;
    }

    private static Round[][] generateRounds(List<Integer> cards) {
        List<Integer> princessCards = new ArrayList<>(cards.subList(0, 3));
        List<Integer> princeCards = new ArrayList<>(cards.subList(3, 5));
        princeCards.add(0);
        Round[][] rounds = new Round[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Round r = new Round(princessCards.get(i), princeCards.get(j));
                rounds[i][j] = r;
            }
        }
        return rounds;
    }

    @Override
    public String toString() {
        return "Game{" +
                "plays=" + plays +
                '}';
    }
}

/**
 * This class represents a play of three rounds
 */
class Play {
    private List<Round> rounds; // Must have 3 rounds

    public Play() {
        this.rounds = new ArrayList<>();
    }

    public void addRound(Round r) {
        rounds.add(r);
    }

    public Optional<Integer> getMinimumCardForPrinceVictory() {
        int princeWins = getPrinceWins();
        switch (princeWins) {
            case 2: return Optional.of(0);  // The play is already won
            case 1: {
                for (Round round: rounds) {
                    if (round.getPrinceCard() == 0 && round.getPrincessCard() < 52) {
                        return Optional.of(round.getPrincessCard() + 1);
                    }
                }
                Optional.empty();
            }
            default: return Optional.empty();
        }
    }

    private int getPrinceWins() {
        int princeWins = 0;
        for (Round round: rounds) {
            if (round.getPrinceCard() > round.getPrincessCard()) {
                princeWins++;
            }
        }
        return princeWins;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    @Override
    public String toString() {
        return "Play{" +
                "rounds=" + rounds +
                '}';
    }
}

/**
 * This class represents a single round
 */
class Round {
    private int princessCard;
    private int princeCard;

    public Round(int princessCard, int princeCard) {
        this.princessCard = princessCard;
        this.princeCard = princeCard;
    }

    public int getPrincessCard() {
        return princessCard;
    }

    public int getPrinceCard() {
        return princeCard;
    }

    @Override
    public String toString() {
        return "Round{" +
                "princessCard=" + princessCard +
                ", princeCard=" + princeCard +
                '}';
    }
}