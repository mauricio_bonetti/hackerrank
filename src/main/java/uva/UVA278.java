package uva;

/**
 * Created by mauricio on 3/31/16.
 */
public class UVA278 {
    public int getMaximumChessPieces(char piece, int[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (squareIsFree(board[i][j]) && !threatsAnyPiece(piece, board, i, j)) {
                    fillSquare(piece, board, i, j);
                }
            }
        }
        int maximumChessPieces = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == 2) {
                    maximumChessPieces++;
                }
            }
        }
        return maximumChessPieces;
    }

    private void fillSquare(char piece, int[][] board, int i, int j) {
        if (piece == 'r') {
            for (int x = 0; x < board.length; x++) {
                board[x][j] = 1;
            }
            for (int x = 0; x < board[i].length; x++) {
                board[i][x] = 1;
            }
        }
        board[i][j] = 2;
    }

    private boolean threatsAnyPiece(char piece, int[][] board, int i, int j) {
        if (piece == 'r') {
            // trasverse rows
            for (int x = 0; x < board.length; x++) {
                if (board[x][j] == 2) {
                    return true;
                }
            }
            // trasverse columns
            for (int x = 0; x < board[i].length; x++) {
                if (board[i][x] == 2) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean squareIsFree(int square) {
        return square == 0;
    }
}
