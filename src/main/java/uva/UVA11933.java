package uva;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by mauricio on 4/2/16.
 */
public class UVA11933 {

    public static void main(String... args) {
        Scanner sc = new Scanner(System.in);
        UVA11933 uva = new UVA11933();
        for(;;) {
            int number = sc.nextInt();
            if (number <= 0) {
                break;
            }
            System.out.print(uva.findDivision(true, number) + " " + uva.findDivision(false, number));
            System.out.println();
        }
    }

    /**
     * This method returns a list containing the indices of a number that are "on".
     * Ex. findTurnedOnBitsIndeces(10) -> {1, 3}
     * @param number
     * @return
     */
    public List<Integer> findTurnedOnBitsIndeces(int number) {
        List<Integer> list = new ArrayList<>();
        int maxIndex = (int) (Math.log(number) / Math.log(2)); // log n base 2
        int shift = 0;
        while (shift <= maxIndex) {
            if (((1 << shift) & number) > 0) {
                list.add(shift);
            }
            shift++;
        }
        return list;
    }

    public int findDivision(boolean evaluateA, int number) {
        final List<Integer> onBitsIndeces = findTurnedOnBitsIndeces(number);
        int div = 0;
        for (int i = evaluateA ? 0 : 1; i < onBitsIndeces.size(); i = i + 2) {
            div = div | (1 << onBitsIndeces.get(i));
        }
        return div;
    }
}
