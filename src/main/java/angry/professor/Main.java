// Solution for https://www.hackerrank.com/challenges/angry-professor
package angry.professor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class TestCase {
    public int studentsQty;
    public int minStudentsQty;
    public List<Integer> arrivalsTimes;
    public String result;

    public TestCase(List<Integer> arrivalsTimes, int studentsQty, int minStudentsQty) {
        this.arrivalsTimes = arrivalsTimes;
        this.studentsQty = studentsQty;
        this.minStudentsQty = minStudentsQty;
    }
}

public class Main {
    private static List<TestCase> tests;

    public static void main(String... args) {
        readInputs();
        evaluateClass();
        printResults();
    }

    private static void printResults() {
        tests.stream().forEach(test -> System.out.println(test.result));
    }

    private static void evaluateClass() {
        tests.forEach(test -> {
            long studentsOnTime = test.arrivalsTimes.stream()
                    .filter(arrivalTime -> arrivalTime.intValue() <= 0)
                    .count();
            test.result = studentsOnTime >= test.minStudentsQty ? "NO" : "YES";
        });
    }

    private static void readInputs() {
        Scanner sc = new Scanner(System.in);
        int testsQty = Integer.parseInt(sc.nextLine());
        tests = new ArrayList<>();
        IntStream.range(0, testsQty).forEach(i -> {
            String[] students = sc.nextLine().split("\\s+");
            int studentsQty = Integer.parseInt(students[0]);
            int threshold = Integer.parseInt(students[1]);
            List<String> arrivalsList = Arrays.asList(sc.nextLine().split("\\s+"));
            List<Integer> arrivalsTimes = arrivalsList.stream()
                    .map(arrival -> Integer.parseInt(arrival))
                    .collect(Collectors.toList());
            tests.add(new TestCase(arrivalsTimes, studentsQty, threshold));
        });
    }
}

