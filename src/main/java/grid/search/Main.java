// https://www.hackerrank.com/challenges/the-grid-search
package grid.search;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {

    public static void main(String... args) {
        Scanner sc = new Scanner(System.in);
        final int testCases = sc.nextInt();
        IntStream.range(0, testCases).mapToObj(testCase -> {
            int i = sc.nextInt();
            int j = sc.nextInt();
            sc.nextLine();
            int[][] grid = readGrid(sc, i, j);
            i = sc.nextInt();
            j = sc.nextInt();
            sc.nextLine();
            int[][] filter = readGrid(sc, i, j);
            return new Main().scan(grid, filter);
        }).forEach(System.out::println);
    }

    private static int[][] readGrid(Scanner sc, int i, int j) {
        int[][] grid = new int[i][j];
        IntStream.range(0, i).forEach(i_e -> {
            List<String> numbers = Arrays.asList(sc.nextLine().split(""));
            IntStream.range(0, numbers.size()).forEach(j_e -> {
               grid[i_e][j_e] = Integer.parseInt(numbers.get(j_e));
            });
        });
        return grid;
    }

    public String scan(int[][] grid, int[][] filter) {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid.length; j++) {
                final boolean result = scan(grid, filter, i, j, 0, 0);
                if (result) {
                    return "YES";
                }
            }
        }
        return "NO";
    }

    private boolean scan(int[][] grid, int[][] filter, int i_g, int j_g, int i_f, int j_f) {
        int g_size_i = grid.length;
        int g_size_j = grid[0].length;
        if (i_g >= g_size_i || j_g >= g_size_j) {
            return false;
        }
        if (grid[i_g][j_g] == filter[i_f][j_f]) {
            int f_size_i = filter.length;
            int f_size_j = filter[0].length;
            if (j_f + 1 < f_size_j) {
                return scan(grid, filter, i_g, j_g + 1, i_f, j_f + 1);
            }
            if (i_f + 1 < f_size_i) {
                return scan(grid, filter, i_g + 1, j_g, i_f + 1, j_f);
            }
            return true;
        }
        return false;
    }
}
